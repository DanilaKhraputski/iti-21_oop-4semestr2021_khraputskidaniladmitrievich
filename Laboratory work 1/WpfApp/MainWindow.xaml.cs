﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VegetableGardening;

namespace WpfApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        private MemoGardener memo;
        private DOMtree parser;

        public DOMtree Parser => parser;

        public MemoGardener Memo => memo;

        public MainWindow()
            : base()
        {
            InitializeComponent();

            parser = new DOMtree();

            memo = new MemoGardener();

            plantList.ItemsSource = memo;

        }

        public void RefreshTable()
        {
            plantList.Items.Refresh();
        }

        private void AddPlant(Object sender, RoutedEventArgs e)
        {
            AddWindow addWindow = new AddWindow(this);

            addWindow.Show();
        }

        private void EditPlant(Object sender, RoutedEventArgs e)
        {
            if (plantList.SelectedIndex == -1)
            {
                MessageBox.Show("not select plant");

                return;
            }

            EditWindow editWindow = new EditWindow(this, plantList.SelectedIndex);

            editWindow.Show();
        }

        private void DeletePlant(Object sender, RoutedEventArgs e)
        {
            if (plantList.SelectedIndex == -1)
            {
                MessageBox.Show("not select plant");

                return;
            }

            memo.DeletePlant(plantList.SelectedIndex);

            parser.EditXmlDocument(pathToXmlDocument.Text, memo);

            RefreshTable();
        }

        private void DownloadXmlDocument(Object sender, RoutedEventArgs e)
        {
            if (!parser.ValidateXmlDocumentByXsdSchema(pathToXmlDocument.Text, pathToXsdScheme.Text))
            {
                MessageBox.Show("Invalid data!");

                return;
            }

            MemoGardener tempMemo = parser.GetMemoGardener(pathToXmlDocument.Text);

            for (var i = 0; i < tempMemo.GetCount(); ++i)
            {
                memo.AddPlant(tempMemo[i]);
            }

            RefreshTable();
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VegetableGardening;

namespace WpfApp
{
    /// <summary>
    /// Логика взаимодействия для AddWindow.xaml
    /// </summary>

    public partial class AddWindow : Window
    {
        private MainWindow mainWindow;

        public AddWindow()
            : base()
        {
            InitializeComponent();

            FillGenreList();

            listVegetableType.SelectedIndex = 6;
        }

        public AddWindow(MainWindow mainWindow)
            : this()
        {
            if (mainWindow == null)
            {
                throw new ArgumentNullException("mainWindow has null value!");
            }

            this.mainWindow = mainWindow;
        }

        private void FillGenreList()
        {

            listVegetableType.Items.Clear();

            Int32 countGenres = (Int32)VegetableType.Count;

            for (var i = 0; i < countGenres; ++i)
            {
                VegetableType genre = (VegetableType)i;

                listVegetableType.Items.Add(genre.ToString());
            }
        }

        private void AddPlant(Object sender, RoutedEventArgs e)
        {
            try
            {
                if (!Plant.IsCorrectSortVegetable(sortVegetable.Text))
                {
                    throw new Exception("Sort is incorrect!\n" +
                        $"Sort: {sortVegetable.Text ?? "null"}");
                }

                if (!Int32.TryParse(height.Text, out Int32 h))
                {
                    throw new Exception("height is incorrect!\n" +
                        $"height: {height.Text ?? "null"}");
                }

                if (!Plant.IsCorrectHeight(h))
                {
                    throw new Exception("Minutes is incorrect!\n" +
                        $"height: {height.Text ?? "null"}");
                }

                if (!Int32.TryParse(gatheringTimeHarvest.Text, out Int32 Th))
                {
                    throw new Exception("Time is incorrect!\n" +
                        $"Time: {gatheringTimeHarvest.Text ?? "null"}");
                }

                if (!Plant.IsCorrectGatheringTimeHarvest(Th))
                {
                    throw new Exception("Time is incorrect!\n" +
                        $"Time: {gatheringTimeHarvest.Text ?? "null"}");
                }

                if (!DateTime.TryParse(recommendedLandingDate.Text, out DateTime d))
                {
                    throw new Exception("Date is incorrect!\n" +
                        $"Date: {recommendedLandingDate.Text ?? "null"}");
                }

                if (!Plant.IsCorrectRecommendedLandingDate(d))
                {
                    throw new Exception("Date is incorrect!\n" +
                        $"Date: {recommendedLandingDate.Text ?? "null"}");
                }

                VegetableType type = (VegetableType)listVegetableType.SelectedIndex;

                Plant plant = new Plant(type, sortVegetable.Text, h, Th, d);

                mainWindow.Memo.AddPlant(plant);

                mainWindow.Parser.EditXmlDocument(mainWindow.pathToXmlDocument.Text, mainWindow.Memo);

                mainWindow.RefreshTable();

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CloseWindow(Object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

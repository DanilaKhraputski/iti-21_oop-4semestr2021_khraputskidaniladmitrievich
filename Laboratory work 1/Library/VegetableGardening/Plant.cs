﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace VegetableGardening
{
    public class Plant
    {
        private VegetableType vegetableType;
        private String sortVegetable;
        private Int32 height;
        private Int32 gatheringTimeHarvest;
        private DateTime recommendedLandingDate;


        public Plant(VegetableType vegetableType, String sortVegetable, Int32 height, Int32 gatheringTimeHarvest, DateTime recommendedLandingDate)
            : base()
        {
            if (!IsCorrectPlant(vegetableType, sortVegetable, height, gatheringTimeHarvest, recommendedLandingDate))
            {
                throw new ArgumentException("Input parameters have incorrect values!\n" +
                    $"Type: {vegetableType.ToString()}\n" +
                    $"Sort: {sortVegetable ?? "null"}\n" +
                    $"Height: {height}\n" +
                    $"Time Harvest: {gatheringTimeHarvest}\n" +
                    $"Landing Date: {recommendedLandingDate}\n");
            }
            this.vegetableType = vegetableType;
            this.sortVegetable = sortVegetable;
            this.height = height;
            this.gatheringTimeHarvest = gatheringTimeHarvest;
            this.recommendedLandingDate = recommendedLandingDate;
        }

        public VegetableType VegetableType => vegetableType;

        public String SortVegetable => sortVegetable;

        public Int32 Height => height;

        public Int32 GatheringTimeHarvest => gatheringTimeHarvest;

        public DateTime RecommendedLandingDate => recommendedLandingDate;

        public static Boolean IsCorrectPlant(VegetableType vegetableType, String sortVegetable, Int32 height, Int32 gatheringTimeHarvest, DateTime recommendedLandingDate)
        {
            if (!IsCorrectVegetableType(vegetableType)) return false;

            if (!IsCorrectSortVegetable(sortVegetable)) return false;

            if (!IsCorrectHeight(height)) return false;

            if (!IsCorrectGatheringTimeHarvest(gatheringTimeHarvest)) return false;

            if (!IsCorrectRecommendedLandingDate(recommendedLandingDate)) return false;

            return true;
        }

        public static Boolean IsCorrectVegetableType(VegetableType vegetableType)
        {
            if (!Enum.IsDefined(typeof(VegetableType), vegetableType)) return false;

            return true;
        }

        public static Boolean IsCorrectSortVegetable(String sortVegetable)
        {
            if (sortVegetable == null) return false;

            String pattern = @"(?x)\A [a-z A-Z 0-9 а-я А-Я \- \( \) \. \s \* ' _]{3,20} \z";

            if (Regex.IsMatch(sortVegetable, pattern)) return true;

            return false;
        }

        public static Boolean IsCorrectHeight(Int32 height)
        {
            if (height < 0 || height > 100) return false;

            return true;

        }

        public static Boolean IsCorrectGatheringTimeHarvest(Int32 gatheringTimeHarvest)
        {
            if (gatheringTimeHarvest < 12 || gatheringTimeHarvest > 365) return false;

            return true;
        }

        public static Boolean IsCorrectRecommendedLandingDate(DateTime recommendedLandingDate)
        {

            return true;
        }

        public void Edit(VegetableType vegetableType, String sortVegetable, Int32 height, Int32 gatheringTimeHarvest, DateTime recommendedLandingDate)
        {
            if (!IsCorrectPlant(vegetableType, sortVegetable, height, gatheringTimeHarvest, recommendedLandingDate))
            {
                throw new ArgumentException("Input parameters have incorrect values!\n" +
                    $"Type: {vegetableType.ToString()}\n" +
                    $"Sort: {sortVegetable ?? "null"}\n" +
                    $"Height: {height}\n" +
                    $"Time Harvest: {gatheringTimeHarvest}\n" +
                    $"Landing Date: {recommendedLandingDate}\n");
            }

            this.vegetableType = vegetableType;
            this.sortVegetable = sortVegetable;
            this.height = height;
            this.gatheringTimeHarvest = gatheringTimeHarvest;
            this.recommendedLandingDate = recommendedLandingDate;
        }

        public override String ToString()
        {
            return $"Type: {vegetableType.ToString()}\n" +
                    $"Sort: {sortVegetable}\n" +
                    $"Height: {height}\n" +
                    $"Time Harvest: {gatheringTimeHarvest}\n" +
                    $"Landing Date: {recommendedLandingDate}\n";
        }

        public override Boolean Equals(Object obj)
        {
            if (obj == null) return false;

            if (obj == this) return true;

            Plant plant = obj as Plant;

            if (plant == null) return false;

            if (plant.vegetableType != this.vegetableType) return false;

            if (plant.sortVegetable != this.sortVegetable) return false;

            if (plant.height != this.height) return false;

            if (plant.gatheringTimeHarvest != this.gatheringTimeHarvest) return false;

            if (plant.recommendedLandingDate != this.recommendedLandingDate) return false;

            return true;
        }

        public override Int32 GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
    }
}

﻿using System;

namespace VegetableGardening
{
    public enum VegetableType : Int32
    {
        Potato,
        Tomato,
        Cucumber,
        Pepper,
        Radishes,
        Salad,
        Count
    }
}
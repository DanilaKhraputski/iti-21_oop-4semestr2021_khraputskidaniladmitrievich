﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace VegetableGardening 
{
    public class MemoGardener : IEnumerable
    {
        private String name;
        private List<Plant> plants;

        public MemoGardener() 
            : base()
        {
            //if (!IsCorrectName(name))
            //{
            //    throw new ArgumentException("name has incorrect value!\n" +
            //        $"name: {name ?? "null"}\n");
            //}

            //this.name = name;

            this.plants = new List<Plant>();
        }

        public IEnumerator GetEnumerator()
        {
            return plants.GetEnumerator();
        }

        public String Name => name;

        public Plant this[Int32 index]
        {
            get
            {
                if (index < 0 || index >= plants.Count)
                {
                    throw new IndexOutOfRangeException($"index has {index} value that < 0 or >= {plants.Count}!");
                }

                return plants[index];
            }
        }

        public Int32 GetCount()
        {
            return plants.Count;
        }

        public static Boolean IsCorrectName(String name)
        {
            if (name == null) return false;

            String pattern = @"(?x)\A [a-z A-Z 0-9 а-я А-Я \- \( \) \. \s \* ' _]{3,20} \z";

            if (Regex.IsMatch(name, pattern)) return true;

            return false;
        }

        public void AddPlant(Plant plant)
        {
            if (plant == null)
            {
                throw new ArgumentNullException(" plant has null value!");
            }

            plants.Add(plant);
        }

        public void DeletePlant(Plant plant)
        {
            if (plant == null)
            {
                throw new ArgumentNullException(" plant has null value!");
            }

            plants.Remove(plant);
        }

        public void DeletePlant(Int32 index)
        {
            if (index < 0 || index >= plants.Count)
            {
                throw new IndexOutOfRangeException($"index must be >= 0 and < {plants.Count}");
            }

            plants.RemoveAt(index);
        }

        public void EditPlant(Int32 index, VegetableType vegetableType, String sortVegetable, Int32 height, Int32 gatheringTimeHarvest, DateTime recommendedLandingDate)
        {
            if (!Plant.IsCorrectPlant(vegetableType, sortVegetable, height, gatheringTimeHarvest, recommendedLandingDate))
            {
                throw new ArgumentException("Input parameters have incorrect values!\n" +
                    $"Type: {vegetableType.ToString()}\n" +
                    $"Sort: {sortVegetable ?? "null"}\n" +
                    $"Height: {height}\n" +
                    $"Time Harvest: {gatheringTimeHarvest}\n" +
                    $"Landing Date: {recommendedLandingDate}\n");
            }

            plants[index].Edit(vegetableType, sortVegetable, height, gatheringTimeHarvest, recommendedLandingDate);
        }

        public override String ToString()
        {
            StringBuilder memoInfo = new StringBuilder($"{name}\n");

            foreach (Plant plant in plants)
            {
                memoInfo.Append(plant.ToString());
            }

            return memoInfo.ToString();
        }

        public override Boolean Equals(Object obj)
        {
            if (obj == null) return false;

            if (obj == this) return true;

            MemoGardener memo = obj as MemoGardener;

            if (memo == null) return false;

            if (memo.name != this.name) return false;

            if (memo.GetCount() != this.GetCount()) return false;

            for (var i = 0; i < memo.GetCount(); ++i)
            {
                if (memo.plants[i] != this.plants[i]) return false;
            }

            return true;
        }

        public override Int32 GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
    }
}

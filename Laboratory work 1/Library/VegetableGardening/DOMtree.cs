﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.IO;

namespace VegetableGardening
{
    public class DOMtree
    {
        public DOMtree() : base() { }

        public void AddRoot(String pathToXmlDocument, Plant plant)
        {
            if (pathToXmlDocument == null)
            {
                throw new ArgumentNullException("pathToNewXmlDocument has null value!");
            }

            if (plant == null)
            {
                throw new ArgumentNullException("Plant has null value!");
            }

            if (!File.Exists(pathToXmlDocument))
            {
                throw new FileNotFoundException("pathToXmlDocument not exist!");
            }

            XmlDocument xmlDocument = new XmlDocument();

            xmlDocument.Load(pathToXmlDocument);

            CreateRoot(xmlDocument, plant);

            xmlDocument.Save(pathToXmlDocument);
        }

        public void EditXmlDocument(String pathToNewXmlDocument, MemoGardener memo)
        {
            if (pathToNewXmlDocument == null)
            {
                throw new ArgumentNullException("pathToNewXmlDocument has null value!");
            }

            if (memo == null)
            {
                throw new ArgumentNullException("Memo has null value!");
            }

            XmlDocument xmlDocument = new XmlDocument();

            XmlDeclaration xmlDeclaration = xmlDocument.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = xmlDocument.DocumentElement;
            xmlDocument.InsertBefore(xmlDeclaration, root);

            XmlElement checklistElement = xmlDocument.CreateElement(String.Empty, "checklist", String.Empty);
            xmlDocument.AppendChild(checklistElement);

            for (var i = 0; i < memo.GetCount(); ++i)
            {
                CreateRoot(xmlDocument, memo[i]);
            }

            xmlDocument.Save(pathToNewXmlDocument);
        }

        private void CreateRoot(XmlDocument xmlDocument, Plant plant)
        {
            if (xmlDocument == null)
            {
                throw new ArgumentNullException("xmlDocument has null value!");
            }

            if (plant == null)
            {
                throw new ArgumentNullException("Plant has null value!");
            }

            if (xmlDocument.DocumentElement == null)
            {
                throw new XmlException("Document has not root!");
            }

            XmlElement root = xmlDocument.DocumentElement;

            XmlElement plantElement = xmlDocument.CreateElement("plant");
            XmlElement vegetableTypeNameElement = xmlDocument.CreateElement("type");
            XmlElement sortVegetableNameElement = xmlDocument.CreateElement("sort");
            XmlElement heightNameElement = xmlDocument.CreateElement("height");
            XmlElement gatheringTimeHarvestNameElement = xmlDocument.CreateElement("timeharvest");
            XmlElement recommendedLandingDate = xmlDocument.CreateElement("landingdate");

            XmlText vegetableTypeNameText = xmlDocument.CreateTextNode(plant.VegetableType.ToString());
            XmlText sortVegetableNameText = xmlDocument.CreateTextNode(plant.SortVegetable);
            XmlText heightNameText = xmlDocument.CreateTextNode(plant.Height.ToString());
            XmlText gatheringTimeHarvestNameText = xmlDocument.CreateTextNode(plant.GatheringTimeHarvest.ToString());
            XmlText recommendedLandingDateNameText = xmlDocument.CreateTextNode(plant.RecommendedLandingDate.ToString());

            vegetableTypeNameElement.AppendChild(vegetableTypeNameText);
            sortVegetableNameElement.AppendChild(sortVegetableNameText);
            heightNameElement.AppendChild(heightNameText);
            gatheringTimeHarvestNameElement.AppendChild(gatheringTimeHarvestNameText);
            recommendedLandingDate.AppendChild(recommendedLandingDateNameText);

            plantElement.AppendChild(vegetableTypeNameElement);
            plantElement.AppendChild(sortVegetableNameElement);
            plantElement.AppendChild(heightNameElement);
            plantElement.AppendChild(gatheringTimeHarvestNameElement);
            plantElement.AppendChild(recommendedLandingDate);
            
            root.AppendChild(plantElement);
        }

        public MemoGardener GetMemoGardener(String pathToXmlDoc)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(pathToXmlDoc);
            MemoGardener memoGardener = new MemoGardener();
            XmlElement root = xmlDoc.DocumentElement;

                foreach (XmlNode node in root)
                {
                    
                    XmlNodeList innerNodes = node.ChildNodes;
                    
                    VegetableType vegetableType = (VegetableType)Enum.Parse(typeof(VegetableType), innerNodes[0].InnerText);
                    String sortVegetable = innerNodes[1].InnerText;
                    Int32 height = Convert.ToInt32(innerNodes[2].InnerText);
                    Int32 gatheringTimeHarvest = Convert.ToInt32(innerNodes[3].InnerText);
                    DateTime recommendedLandingDate = Convert.ToDateTime(innerNodes[4].InnerText);
                    memoGardener.AddPlant(new Plant(vegetableType, sortVegetable, height, gatheringTimeHarvest, recommendedLandingDate));
                }

            return memoGardener;       
        }

        public Boolean ValidateXmlDocumentByXsdSchema(String pathToXmlDocument, String pathToXsdSchema)
        {
            if (pathToXmlDocument == null)
            {
                throw new ArgumentNullException("pathToNewXmlDocument has null value!");
            }

            if (pathToXsdSchema == null)
            {
                throw new ArgumentNullException("pathToXsdSchema has null value!");
            }

            if (!File.Exists(pathToXmlDocument))
            {
                throw new FileNotFoundException("pathToXmlDocument not exist!");
            }

            if (!File.Exists(pathToXsdSchema))
            {
                throw new FileNotFoundException("pathToXsdSchema not exist!");
            }

            XmlSchemaSet xmlSchema = new XmlSchemaSet();

            xmlSchema.Add(null, pathToXsdSchema);

            XDocument xmlDocument = XDocument.Load(pathToXmlDocument);

            try
            {
                xmlDocument.Validate(xmlSchema, (sender, e) =>
                {
                    throw new XmlException(e.Message);
                });
            }
            catch (XmlException)
            {
                return false;
            }

            return true;
        }
    }
}

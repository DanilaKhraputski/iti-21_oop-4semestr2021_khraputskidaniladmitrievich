﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LinqString;
using System.Collections.Generic;
using System.Linq;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestLinqString()
        {
            string TextFileTest = @"H:\Саня_ООП\LAb9\ConsoleApp\ConsoleApp\bin\Debug\TextFileTest.txt";
            var expected = new LinqClass(TextFileTest);
            List<string> a = new List<string> { "а", "аб", "абв", "абвг", "абв", "авбг" };
            Assert.IsTrue(a.SequenceEqual(expected.ParseString));
        }
    }
}

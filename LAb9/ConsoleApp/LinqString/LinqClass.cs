﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqString
{
    public class LinqClass
    {
        public List<String> ParseString { get; private set; }

        public LinqClass(string filePath)
        {
            ExecuteLINQTask(filePath);
        }

        private void ExecuteLINQTask(string filePath)
        {
            ParseString = File.ReadAllText(filePath).ToLower().Split(new char[] { '.', '?', '!', ' ', ';', ':', ',' },
                                                    StringSplitOptions.RemoveEmptyEntries).Where(t => t.Count() <= 4).ToList();

        }
    }
}

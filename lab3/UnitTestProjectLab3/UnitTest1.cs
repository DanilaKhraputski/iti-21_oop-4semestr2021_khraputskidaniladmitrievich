﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace ClassLibraryLAb3.Tests
{
    /// <summary>
    ///
    /// </summary>
    [TestClass()]
    public class UnitTest1
    {
        /// <summary>
        /// Serializes the object test.
        /// </summary>
        [TestMethod()]
        public void SerializeObjectTest()
        {
            object[] furnitures = new Furniture[] { new Table( "1",TypeFurniture.металл, 1000, 12, "Russia", true, "10x20x21"), new Sofa( "1",TypeFurniture.дерево, 302, 92, "Russia", false, "50x50x101") };
            SerializationXML.SerializeObject(typeof(Furniture[]), furnitures, "Furniture.xml");
            Furniture[] newFurniture = (Furniture[])SerializationXML.DeserializeObject(typeof(Furniture[]), "Furniture.xml");
            foreach (var furniture in newFurniture)
            {
                if (furniture is Table)
                    Assert.IsTrue(furniture.GetType() == typeof(Table));
                else Assert.IsTrue(furniture.GetType() == typeof(Sofa));
            }
        }

        /// <summary>
        /// Deserializes the object test.
        /// </summary>
        [TestMethod()]
        public void DeserializeObjectTest()
        {
            Furniture[] newFurniture = (Furniture[])SerializationXML.DeserializeObject(typeof(Furniture[]), "Furniture.xml");
            foreach (var furniture in newFurniture)
            {
                if (furniture is Table)
                    Assert.IsTrue(furniture.GetType() == typeof(Table));
                else Assert.IsTrue(furniture.GetType() == typeof(Sofa));
            }
        }


        /// <summary>
        /// Invokes the metho test.
        /// </summary>
        [TestMethod()]
        public void InvokeMethoTest()
        {
            User user = new User("Pavel", 23);
            Assert.IsTrue((int)user.InvokeMethod("User", new object[] { user.Name, user.Age }, "Payment", new object[] { 10, 200 }) == 2000);
        }

        /// <summary>
        /// Converts to stringtest.
        /// </summary>
        [TestMethod()]
        public void ToStringTest()
        {
            User user = new User("Pavel", 23);
            Assert.IsTrue(user.ToString() == "Имя:Pavel Возраст:23");
        }
    }
}
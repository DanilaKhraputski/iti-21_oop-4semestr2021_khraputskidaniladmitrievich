﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace ClassLibraryLAb3
{
    /// <summary>
    /// класс серилизациии XML
    /// </summary>
    public static class SerializationXML
    {
        /// <summary>
        /// Serializes the object.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="obj">The object.</param>
        /// <param name="path">The path.</param>
        public static void SerializeObject(Type type, object[] obj, string path)
        {
            
                XmlSerializer formatter = new XmlSerializer(type);
                using (FileStream fs = new FileStream($"{path}.xml", FileMode.OpenOrCreate))
                {
                    formatter.Serialize(fs, obj);
                }
        }
        /// <summary>
        /// Serializes the object.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="obj">The object.</param>
        /// <param name="path">The path.</param>
        public static void SerializeObject(Type type, object obj, string path)
        {

            XmlSerializer xml = new XmlSerializer(type);
            using (FileStream fs = new FileStream($"{path}.xml", FileMode.OpenOrCreate))
            {
                xml.Serialize(fs, obj);
            }
        }
        /// <summary>
        /// Deserializes the object.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public static object DeserializeObject(Type type, string path)
        {
            XmlSerializer formatter = new XmlSerializer(type);
            using (FileStream fs = new FileStream($"{path}.xml", FileMode.Open))
            {
                object obj = (object)formatter.Deserialize(fs);
                return obj;
            }
        }
    }
}
﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.Json;

namespace ClassLibraryLAb3
{
    /// <summary>
    /// серилизация Json
    /// </summary>
    public static class SerializationJSON
    {
        /// <summary>
        /// Serializes the object json.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public static string[] SerializeObjectJSON( object[] obj)
        {
            string[] json = new string[obj.Length];
            for (int i=0;i<obj.Length;i++)
            {
                json[i] = System.Text.Json.JsonSerializer.Serialize<object>(obj[i]);
            }
            return json;
        }


        /// <summary>
        /// Deserializes the object json.
        /// </summary>
        /// <param name="serialized">The serialized.</param>
        /// <returns></returns>
        public static GroupFurnitures DeserializeObjectJSON(string[] serialized)
        {

            GroupFurnitures furnituresJson = new GroupFurnitures();
            for (int i = 0; i < serialized.Length; i++)
            {
                if (serialized[i].Split(new char[] { '"' })[3] == "Table")
                {
                    furnituresJson.FurnituresList.Add(JsonConvert.DeserializeObject<Table>(serialized[i]));
                }
                else { furnituresJson.FurnituresList.Add(JsonConvert.DeserializeObject<Sofa>(serialized[i])); }

            }
            return furnituresJson;
        }

    }
}
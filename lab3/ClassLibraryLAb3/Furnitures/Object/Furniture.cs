﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ClassLibraryLAb3
{
    /// <summary>
    /// Класс мебели
    /// </summary>
    [Serializable]
    //[XmlInclude(typeof(Table)), XmlInclude(typeof(Sofa))] 

    //[ JsonConverter(typeof(Table) )] // JsonConverter(typeof(Sofa))

    public abstract class Furniture
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Furniture"/> class.
        /// </summary>
        public Furniture()
        {

        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Furniture"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="typeFurniture">The type furniture.</param>
        /// <param name="price">The price.</param>
        /// <param name="discount">The discount.</param>
        /// <param name="manufacturer">The manufacturer.</param>
        /// <param name="stockAvailability">if set to <c>true</c> [stock availability].</param>
        /// <param name="size">The size.</param>
        /// <exception cref="ArgumentNullException">
        /// id
        /// or
        /// manufacturer
        /// or
        /// size
        /// </exception>
        public Furniture(string id, TypeFurniture typeFurniture, decimal price, int discount, string manufacturer, bool stockAvailability, string size)
        {
            Id = id ?? throw new ArgumentNullException(nameof(id));
            TypeFurniture = typeFurniture;
            Price = price;
            Discount = discount;
            Manufacturer = manufacturer ?? throw new ArgumentNullException(nameof(manufacturer));
            StockAvailability = stockAvailability;
            Size = size ?? throw new ArgumentNullException(nameof(size));
        }
        /// <summary>
        /// Strings to type furniture.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        /// <exception cref="Exception">нет такого вида мебели</exception>
        public static TypeFurniture StringToTypeFurniture(string str)
        {

            TypeFurniture furniture;
            switch (str.ToLower())
            {
                case "дерево": furniture = TypeFurniture.дерево; break;
                case "гибрид": furniture = TypeFurniture.гибрид; break;
                case "металл": furniture = TypeFurniture.металл; break;
                case "пластмасса": furniture = TypeFurniture.пластмасса; break;
                default: throw new Exception("нет такого вида мебели");
            }
            return furniture;
        }
        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public string Type { get { return GetType().Name; } }
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Id { get; set; }
        /// <summary>
        /// Gets or sets the type furniture.
        /// </summary>
        /// <value>
        /// The type furniture.
        /// </value>
        public TypeFurniture TypeFurniture { get; set; }
        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>
        /// The price.
        /// </value>
        public decimal Price { get; set; }
        /// <summary>
        /// Gets or sets the discount.
        /// </summary>
        /// <value>
        /// The discount.
        /// </value>
        public int Discount { get; set; }
        /// <summary>
        /// Gets or sets the manufacturer.
        /// </summary>
        /// <value>
        /// The manufacturer.
        /// </value>
        public string Manufacturer { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [stock availability].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [stock availability]; otherwise, <c>false</c>.
        /// </value>
        public bool StockAvailability { get; set; }
        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        public string Size { get; set; }
        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return $"{Id} {Type} {TypeFurniture} {Price} {Discount} {Manufacturer} {StockAvailability} {Size}";
        }
        /// <summary>
        /// Types the furniture is int.
        /// </summary>
        /// <param name="i">The i.</param>
        /// <returns></returns>
        public static TypeFurniture TypeFurnitureIsInt(int i)
        {
            TypeFurniture type;
            switch(i)
            {
                case 1: type = TypeFurniture.дерево;break;
                case 2: type = TypeFurniture.пластмасса; break;
                case 3: type = TypeFurniture.металл; break;
                case 4: type = TypeFurniture.гибрид; break;
                default: type = TypeFurniture.дерево;break;
            }
            return type;
        }
    }
}

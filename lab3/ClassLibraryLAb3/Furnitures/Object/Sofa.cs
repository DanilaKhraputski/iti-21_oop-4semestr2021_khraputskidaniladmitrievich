﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryLAb3
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="ClassLibraryLAb3.Furniture" />
     [Serializable]
    public class Sofa : Furniture
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="Sofa"/> class.
        /// </summary>
        public Sofa()
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Sofa"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="typeFurniture">The type furniture.</param>
        /// <param name="price">The price.</param>
        /// <param name="discount">The discount.</param>
        /// <param name="manufacturer">The manufacturer.</param>
        /// <param name="stockAvailability">if set to <c>true</c> [stock availability].</param>
        /// <param name="size">The size.</param>
        public Sofa(string id,TypeFurniture typeFurniture, decimal price, int discount, string manufacturer, bool stockAvailability, string size) : base(id, typeFurniture, price, discount, manufacturer, stockAvailability, size)
        {

        }
    }
}

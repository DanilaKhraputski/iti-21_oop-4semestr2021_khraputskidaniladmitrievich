﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryLAb3
{
    /// <summary>
    /// The sofa creator.
    /// </summary>
    [Serializable]
    public class SofaCreator : CreatorFurniture
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SofaCreator"/> class.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="typeFurniture"></param>
        /// <param name="price"></param>
        /// <param name="discount"></param>
        /// <param name="manufacturer"></param>
        /// <param name="stockAvailability"></param>
        /// <param name="size"></param>
        public SofaCreator(string id, TypeFurniture typeFurniture, decimal price, int discount, string manufacturer, bool stockAvailability, string size) 
        {
            _id = id;
            _typeFurniture = typeFurniture;
            _price = price;
            _discount = discount;
            _manufacturer = manufacturer;
            _stockAvailability = stockAvailability;
            _size = size;
        }

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns></returns>
        public override Furniture Create()
        {
            
            return new Sofa(_id,_typeFurniture, _price, _discount, _manufacturer, _stockAvailability, _size);
        }

   
    }
}

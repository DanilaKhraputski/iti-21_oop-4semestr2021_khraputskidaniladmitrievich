﻿using System;

namespace ClassLibraryLAb3
{
    /// <summary>
    ///
    /// </summary>
    [Serializable]
    public abstract class CreatorFurniture
    {
        private protected TypeFurniture _typeFurniture;
        private protected string _id;
        private protected decimal _price;
        private protected int _discount;
        private protected string _manufacturer;
        private protected bool _stockAvailability;
        private protected string _size;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreatorFurniture"/> class.
        /// </summary>
        public CreatorFurniture()
        {

        }

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns></returns>
        public abstract Furniture Create();
    }
}
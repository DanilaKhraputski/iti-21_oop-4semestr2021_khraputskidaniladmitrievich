﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryLAb3
{

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="ClassLibraryLAb3.CreatorFurniture" />
    [Serializable]
    public class TableCreator : CreatorFurniture
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TableCreator"/> class.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="typeFurniture"></param>
        /// <param name="price"></param>
        /// <param name="discount"></param>
        /// <param name="manufacturer"></param>
        /// <param name="stockAvailability"></param>
        /// <param name="size"></param>
        public TableCreator(string id, TypeFurniture typeFurniture, decimal price, int discount, string manufacturer, bool stockAvailability, string size) 
        {
            _id = id;
            _typeFurniture = typeFurniture;
            _price = price;
            _discount = discount;
            _manufacturer = manufacturer;
            _stockAvailability = stockAvailability;
            _size = size;
        }
        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns></returns>
        public override Furniture Create()
        {
            return new Table(_id,_typeFurniture,_price,_discount,_manufacturer,_stockAvailability,_size);
        }

       
    }
}

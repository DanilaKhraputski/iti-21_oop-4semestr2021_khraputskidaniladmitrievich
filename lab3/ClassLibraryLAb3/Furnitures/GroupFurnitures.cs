﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ClassLibraryLAb3
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]

    public class GroupFurnitures
    {
        /// <summary>
        /// Gets or sets the furnitures list.
        /// </summary>
        /// <value>
        /// The furnitures list.
        /// </value>
          [XmlArray("FurnitureList")]
        [XmlArrayItem("Sofa", typeof(Sofa))]
        [XmlArrayItem("Table", typeof(Table))]
        public List<Furniture> FurnituresList { get; set; } = new List<Furniture>();
    }
}

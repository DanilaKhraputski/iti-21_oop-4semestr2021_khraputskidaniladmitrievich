﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryLAb3
{
    /// <summary>
    /// типы мебели
    /// </summary>
    public enum TypeFurniture
    {
        /// <summary>
        /// The ДЕРЕВО
        /// </summary>
        дерево,

        /// <summary>
        /// The ПЛАСТМАССА
        /// </summary>
        пластмасса,

        /// <summary>
        /// The МЕТАЛЛ
        /// </summary>
        металл,

        /// <summary>
        /// The ГИБРИД
        /// </summary>
        гибрид
    }
}

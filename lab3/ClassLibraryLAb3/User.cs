﻿using System;
using System.Reflection;
using System.Text;

namespace ClassLibraryLAb3
{
    /// <summary>
    /// классс пользователя
    /// </summary>
    public class User
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; private set; }

        private string _name;

        private int _age;
        /// <summary>
        /// Gets the age.
        /// </summary>
        /// <value>
        /// The age.
        /// </value>
        public int Age { get; private set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        /// <param name="n">The n.</param>
        /// <param name="a">a.</param>
        public User(string n, int a)
        {
            Name = n;
            Age = a;
        }

        private User()
        {
        }

        private void Display()
        {
            Console.WriteLine($"Имя: {Name}  Возраст: {Age}");
        }

        private int Payment(int hours, int perhour)
        {
            return hours * perhour;
        }
        /// <summary>
        /// Gets all private field.
        /// </summary>
        /// <returns></returns>
        public string GetAllPrivateField()
        {
            Type myType = typeof(User);

            StringBuilder result = new StringBuilder("Все скрытые поля:\n");

            foreach (FieldInfo field in myType.GetFields(BindingFlags.Instance | BindingFlags.NonPublic))
            {
                result.Append($"{field.FieldType.Name} {field.Name}\n");
            }

            return result.ToString();
        }
        /// <summary>
        /// Gets all method.
        /// </summary>
        /// <returns></returns>
        public string GetAllMethod()
        {
            Type myType = typeof(User);
            StringBuilder result = new StringBuilder("Все методы:\n");
            foreach (var method in myType.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public))
            {
                if (method.IsStatic)
                    result.Append("static ");
                if (method.IsVirtual)
                    result.Append("virtual");
                result.Append($"{method.ReturnType.Name} {method.Name} (");
                ParameterInfo[] parameters = method.GetParameters();
                for (int i = 0; i < parameters.Length; i++)
                {
                    result.Append($"{parameters[i].ParameterType.Name} {parameters[i].Name}");
                    if (i + 1 < parameters.Length) result.Append(",");
                }
                result.Append(")\n");
            }
            return result.ToString();
        }
        /// <summary>
        /// Invokes the method.
        /// </summary>
        /// <param name="_class">The class.</param>
        /// <param name="construct">The construct.</param>
        /// <param name="method">The method.</param>
        /// <param name="Args">The arguments.</param>
        /// <returns></returns>
        /// <exception cref="Exception">Метод не найден</exception>
        public object InvokeMethod(string _class, object[] construct, string method, object[] Args)
        {

            Assembly asm = Assembly.LoadFrom(@"ClassLibraryLAb3.dll");
            Type t = asm.GetType($"ClassLibraryLAb3.{_class}", true, true);
            object obj = Activator.CreateInstance(t, construct);
            MethodInfo method1 = t.GetMethod($"{method}", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            if(method1==null) throw new Exception("Метод не найден");
            object result = method1.Invoke(obj, Args);
                if (result != null)
                {
                    return result;
                }
                else return obj;
        }
        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return $"Имя:{Name} Возраст:{Age}";
        }
    }
}
﻿using ClassLibraryLAb3;
using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace ConsoleAppLAb3
{
    internal class Program
    {
        private static void Main(string[] Args)
        {
            User user = new User("Vova", 36);
            int exit = 0;
            string method;
            int countArgs = 0;
            while (exit != 3)
            {
                Console.Clear();
                Console.WriteLine("1.Вызов метода\n2.Серилизация и Десерилизация JSON и XML\n3.Выход");
                exit = InputInt(exit);
                while (exit == 1)
                {
                    Console.Clear();
                    Console.WriteLine(user.GetAllPrivateField());
                    Console.WriteLine(user.GetAllMethod());
                    Console.WriteLine("Выберите Метод");
                    method = Console.ReadLine();
                    Console.WriteLine("Введите кол-во аргументов");
                    countArgs = InputInt(countArgs);
                    object[] args = null;
                    if (countArgs != 0)
                    {
                        args = new object[countArgs];
                        for (int i = 0; i < countArgs; i++)
                        {
                            Console.WriteLine($"Ввод {i + 1} аргумента");
                            object arg = Console.ReadLine();
                            if (int.TryParse((string)arg, out int tryInt) == true)
                            {
                                args[i] = tryInt;
                            }
                            else args[i] = arg;
                        }
                    }
                    Console.ReadLine();
                    try
                    {
                        if (user.InvokeMethod("User", new object[] { user.Name, user.Age }, method, args) as User != null)
                        {
                            Console.WriteLine(user);
                            user = user.InvokeMethod("User", new object[] { user.Name, user.Age }, method, args) as User;
                            Console.WriteLine(user);
                        }
                        else Console.WriteLine($"Метод {method} результат:{user.InvokeMethod("User", new object[] { user.Name, user.Age }, method, args)}");
                        Console.ReadLine();
                    }
                    catch (Exception ex) { Console.WriteLine(ex.Message); }
                }
                while (exit == 2)
                {
                    File.Delete("Furniture.xml");
                    GroupFurnitures group = CreateObject();
                    SerializationXML.SerializeObject(typeof(GroupFurnitures), group, "Furniture");
                    using (FileStream fs = new FileStream("Furniture.xml", FileMode.OpenOrCreate))
                    {
                        XmlSerializer formatter = new XmlSerializer(typeof(GroupFurnitures));
                        formatter.Serialize(fs, group);
                    }
                    using (FileStream fs = new FileStream("Furniture.xml", FileMode.OpenOrCreate))
                    {
                        byte[] array = new byte[fs.Length];
                        // считываем данные
                        fs.Read(array, 0, array.Length);
                        string textFromFile = System.Text.Encoding.UTF8.GetString(array);
                        Console.WriteLine($"Текст из файла:\n {textFromFile}");
                    }
                    GroupFurnitures creator = (GroupFurnitures)SerializationXML.DeserializeObject(typeof(GroupFurnitures), "Furniture");
                    Console.WriteLine("------ XML DeserializeObjectXML");
                    foreach (var furniture in creator.FurnituresList)
                    {
                        Console.WriteLine($"{furniture}");
                    }
                    Console.WriteLine("------  SerializationJSON");
                    string[] serializationJSON = SerializationJSON.SerializeObjectJSON(group.FurnituresList.ToArray());
                    StringBuilder str = new StringBuilder();
                    foreach (var i in serializationJSON)
                    {
                        str.Append(i);
                    }
                    File.WriteAllText("Furniture.json", str.ToString());
                    string serializationJSONFile = File.ReadAllText("Furniture.json");
                    Console.WriteLine("_____" + serializationJSONFile + "_______");
                    GroupFurnitures furnituresJson = SerializationJSON.DeserializeObjectJSON(serializationJSON);
                    Console.WriteLine("------ JSON DeserializeObjectJSON");
                    foreach (var fur in furnituresJson.FurnituresList)
                    {
                        Console.WriteLine(fur);
                    }
                    exit = -1;
                    Console.ReadLine();
                }
            }
            Console.ReadLine();
        }

        private static GroupFurnitures CreateObject()
        {
            GroupFurnitures group = new GroupFurnitures();
            Random ran = new Random();
            for (int i = 0; i < 20; i++)
            {
                CreatorFurniture creatorTable = new TableCreator(i.ToString(), Furniture.TypeFurnitureIsInt(ran.Next(1, 4)), ran.Next(1, 1000), ran.Next(1, 80), "Russia", ran.Next(-10, 10) > 0 ? true : false, $"{ ran.Next(0, 100)}x{ran.Next(0, 100)}x{ran.Next(0, 100)}");
                CreatorFurniture creatorSofa = new SofaCreator(i.ToString(), Furniture.TypeFurnitureIsInt(ran.Next(1, 4)), ran.Next(1, 1000), ran.Next(1, 80), "Russia", ran.Next(-10, 10) > 0 ? true : false, $"{ ran.Next(0, 100)}x{ran.Next(0, 100)}x{ran.Next(0, 100)}");
                if (ran.Next(-10, 10) > 0)
                {
                    group.FurnituresList.Add(creatorTable.Create());
                }
                else
                {
                    group.FurnituresList.Add(creatorSofa.Create());
                }
            }
            return group;
        }

        private static int InputInt(int exit)
        {
            bool result = false;
            while (result != true)
            {
                result = int.TryParse(Console.ReadLine(), out exit);
            }
            return exit;
        }
    }
}
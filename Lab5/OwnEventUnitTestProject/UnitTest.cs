﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OwnEventClassLibrary;
using System.Drawing;
using System.Windows.Forms;

namespace OwnEventUnitTestProject
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestMethodFirstSwipe()
        {
            Point firstPoint = new Point(1, 1);
            Point secondPoint = new Point(4, 4);

            MyRectangle rectangle = new MyRectangle(firstPoint, secondPoint, new Panel().CreateGraphics(), Color.Red);

            Assert.IsTrue(secondPoint.X == rectangle.RightBottomPoint.X && secondPoint.Y == rectangle.RightBottomPoint.Y);
        }

        [TestMethod]
        public void TestMethodSecondSwipe()
        {
            Point firstPoint = new Point(4, 4);
            Point secondPoint = new Point(1, 1);

            MyRectangle rectangle = new MyRectangle(firstPoint, secondPoint, new Panel().CreateGraphics(), Color.Red);

            Assert.IsTrue(secondPoint.X == rectangle.LeftTopPoint.X && secondPoint.Y == rectangle.LeftTopPoint.Y);
        }

        [TestMethod]
        public void TestMethodThirdSwipe()
        {
            Point firstPoint = new Point(1, 4);
            Point secondPoint = new Point(4, 1);

            Point result = new Point(1, 1);

            MyRectangle rectangle = new MyRectangle(firstPoint, secondPoint, new Panel().CreateGraphics(), Color.Red);

            Assert.IsTrue(result.X == rectangle.LeftTopPoint.X && result.Y == rectangle.LeftTopPoint.Y);
        }

        [TestMethod]
        public void TestMethodFourSwipe()
        {
            Point firstPoint = new Point(4, 1);
            Point secondPoint = new Point(1, 4);

            Point result = new Point(1, 1);

            MyRectangle rectangle = new MyRectangle(firstPoint, secondPoint, new Panel().CreateGraphics(), Color.Red);

            Assert.IsTrue(result.X == rectangle.LeftTopPoint.X && result.Y == rectangle.LeftTopPoint.Y);
        }

        [TestMethod]
        public void TestMethodCheckAvaliablityFalse()
        {
            MyRectangleCollection collection = new MyRectangleCollection();

            Point firstPoint = new Point(1, 1);
            Point secondPoint = new Point(2, 2);
            collection.Add(firstPoint, secondPoint, new Panel().CreateGraphics(), Color.Red);

            firstPoint = new Point(3, 3);
            secondPoint = new Point(4, 4);
            collection.Add(firstPoint, secondPoint, new Panel().CreateGraphics(), Color.Red);

            firstPoint = new Point(3, 3);
            secondPoint = new Point(4, 4);
            collection.Add(firstPoint, secondPoint, new Panel().CreateGraphics(), Color.Red);

            Assert.IsTrue(collection.Count != 3);
        }

        [TestMethod]
        public void TestMethodCheckAvaliablityTrue()
        {
            MyRectangleCollection collection = new MyRectangleCollection();

            Point firstPoint = new Point(1, 1);
            Point secondPoint = new Point(2, 2);
            collection.Add(firstPoint, secondPoint, new Panel().CreateGraphics(), Color.Red);

            firstPoint = new Point(3, 3);
            secondPoint = new Point(4, 4);
            collection.Add(firstPoint, secondPoint, new Panel().CreateGraphics(), Color.Red);

            firstPoint = new Point(3, 1);
            secondPoint = new Point(4, 2);
            collection.Add(firstPoint, secondPoint, new Panel().CreateGraphics(), Color.Red);

            Assert.IsTrue(collection.Count == 3);
        }
    }
}

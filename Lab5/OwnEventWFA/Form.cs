﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using OwnEventClassLibrary;

namespace OwnEventWFA
{
    public partial class Form : System.Windows.Forms.Form
    {
        MyRectangleCollection rectangles;
        List<Point> points;

        Graphics graphics;

        bool IsAdded;

        public Form()
        {
            InitializeComponent();
            rectangles = new MyRectangleCollection();
            points = new List<Point>();

            graphics = PaintPanel.CreateGraphics();
            KeyPreview = true;

            IsAdded = false;
        }

        private void PaintPanel_MouseClick(object sender, MouseEventArgs e)
        {
            if (IsAdded)
            {
                points.Add(new Point(e.X, e.Y));
                Refresh();

                if (points.Count == 2)
                {
                    Random random = new Random();

                    rectangles.Add(points[0], points[1], graphics, Color.FromKnownColor((KnownColor)random.Next(34, 72)));
                    IsAdded = false;

                    //rectangles[rectangles.Count - 1].MouseDoubleClickEvent += PaintPanel_MouseDoubleClick;
                    //rectangles[rectangles.Count - 1].KeyboardMoveEvent += Form_KeyUp;

                    points.Clear();
                    PaintPanel.Refresh();
                }
            }
        }

        private void PaintPanel_Paint(object sender, PaintEventArgs e)
        {
            for (int i = 0; i < points.Count; i++)
                graphics.DrawRectangle(new Pen(Color.Black, 2), points[i].X, points[i].Y, 2, 2);
            for (int i = 0; i < rectangles.Count; i++)
                rectangles[i].Draw();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            IsAdded = true;
        }

        private void PaintPanel_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < rectangles.Count; i++)
            {


                rectangles[i].ResetColor();
                rectangles[i].Selected(sender, e);
            }
            Refresh();
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            graphics.Clear(Color.White);
            rectangles.Clear();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams handleParam = base.CreateParams;
                handleParam.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED       
                return handleParam;
            }
        }

        private void PaintPanel_MouseMove(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < rectangles.Count; i++)
            {

                rectangles[i].Selected(sender, e);
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OwnEventClassLibrary
{
    public class MyRectangleCollection
    {
        private List<MyRectangle> rectangles;

        public MyRectangle this[int index]
        {
            get
            {
                return rectangles[index];
            }
            set
            {
                rectangles[index] = value;
            }
        }

        public MyRectangleCollection()
        {
            rectangles = new List<MyRectangle>();
        }

        public int Count => rectangles.Count;

        public List<MyRectangle> GetList { get { return rectangles; } }

        public void Add(Point firstPoint, Point secondPoint, Graphics graphics, Color color)
        {
            MyRectangle myRectangle = new MyRectangle(firstPoint, secondPoint, graphics, color);
            if (CheckAvaliablity(myRectangle))
            {
                rectangles.Add(new MyRectangle(firstPoint, secondPoint, graphics, color));
                rectangles.Last().Draw();
            }
            else
            {
                MessageBox.Show("Нельзя добавить фигуру.");
            }
        }

        public bool CheckAvaliablity(MyRectangle myRectangle)
        {
            foreach (MyRectangle rectangle in rectangles)
            {
                // Внутри фиугры
                if (rectangle.IsHavePoint(myRectangle.LeftTopPoint))
                    return false;
                if (rectangle.IsHavePoint(myRectangle.LeftBottomPoint))
                    return false;
                if (rectangle.IsHavePoint(myRectangle.RightTopPoint))
                    return false;
                if (rectangle.IsHavePoint(myRectangle.RightBottomPoint))
                    return false;

                // Внутры новой фигуры
                if (myRectangle.IsHavePoint(rectangle.LeftTopPoint))
                    return false;
                if (myRectangle.IsHavePoint(rectangle.LeftBottomPoint))
                    return false;
                if (myRectangle.IsHavePoint(rectangle.RightTopPoint))
                    return false;
                if (myRectangle.IsHavePoint(rectangle.RightBottomPoint))
                    return false;
            }
            return true;
        }

        public void Clear()
        {
            rectangles.Clear();
        }
    }
}

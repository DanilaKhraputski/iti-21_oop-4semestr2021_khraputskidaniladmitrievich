﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using ToolTip = System.Windows.Forms.ToolTip;

namespace OwnEventClassLibrary
{
    public class MyRectangle
    {
        private Point _leftTopPoint;
        private Point _leftBottomPoint;
        private Point _rightTopPoint;
        private Point _rightBottomPoint;

        public Point LeftTopPoint { get { return _leftTopPoint; } }
        public Point LeftBottomPoint { get { return _leftBottomPoint; } }
        public Point RightTopPoint { get { return _rightTopPoint; } }
        public Point RightBottomPoint { get { return _rightBottomPoint; } }

        public Graphics Graphics { get; }
        public Color Color { get; private set; }
        public Color SelectedColor { get; private set; }

        public bool IsSelected { get; set; }

        public delegate void MouseDoubleClick(object sender, MouseEventArgs args);
        public delegate void KeyboardMove(object sender, KeyEventArgs args);

        public event MouseEventHandler MouseMove;

        public MyRectangle(Point firstPoint, Point secondPoint, Graphics graphics, Color color)
        {
            _leftTopPoint = firstPoint;
            _rightBottomPoint = secondPoint;
            SortPoints();

            _leftBottomPoint = new Point(_leftTopPoint.X, _rightBottomPoint.Y);
            _rightTopPoint = new Point(_rightBottomPoint.X, _leftTopPoint.Y);

            Graphics = graphics;
            Color = color;
            SelectedColor = color;

            IsSelected = false;

            MouseMove += SelectedDelegate;
        }

        private void SortPoints()
        {
            // Если первая точка находится в правом нижнем углу, а вторая - в левом верхнем;
            if (LeftTopPoint.X > RightBottomPoint.X && LeftTopPoint.Y > RightBottomPoint.Y)
            {
                Swap("x");
                Swap("y");
            }
            // Если первая точка находится в левом нижнем углу, а вторая - в правом верхнем;
            if (LeftTopPoint.X < RightBottomPoint.X && LeftTopPoint.Y > RightBottomPoint.Y)
                Swap("y");
            // Если первая точка находисят в правом верхнем углу, а вторая - в левом нижнем;
            if (LeftTopPoint.X > RightBottomPoint.X && LeftTopPoint.Y < RightBottomPoint.Y)
                Swap("x");
        }

        private void Swap(string coordinate)
        {
            int tmp;
            switch (coordinate)
            {
                case "x":
                    tmp = _leftTopPoint.X;
                    _leftTopPoint.X = _rightBottomPoint.X;
                    _rightBottomPoint.X = tmp;
                    break;
                case "y":
                    tmp = _leftTopPoint.Y;
                    _leftTopPoint.Y = _rightBottomPoint.Y;
                    _rightBottomPoint.Y = tmp;
                    break;
            }
        }

        public void Draw()
        {
            int height = RightBottomPoint.Y - LeftTopPoint.Y;
            int width = RightBottomPoint.X - LeftTopPoint.X;
            Graphics.DrawRectangle(new Pen(SelectedColor), LeftTopPoint.X, LeftTopPoint.Y, width, height);
            Graphics.FillRectangle(new SolidBrush(SelectedColor), LeftTopPoint.X, LeftTopPoint.Y, width, height);
        }

        public bool IsHavePoint(Point point)
        {
            if (point.X >= LeftTopPoint.X && point.X <= RightBottomPoint.X)
            {
                if (point.Y >= LeftTopPoint.Y && point.Y <= RightBottomPoint.Y)
                    return true;
            }
            return false;
        }

        public void Selected(object sender, MouseEventArgs args)
        {
            MouseMove?.Invoke(sender, args);
        }

        private void SelectedDelegate(object sender, MouseEventArgs args)
        {
            if (IsHavePoint(new Point(args.X, args.Y)))
            {
                SelectedColor = Color.FromArgb(140, Color);
                int height = RightBottomPoint.Y - LeftTopPoint.Y;
                int width = RightBottomPoint.X - LeftTopPoint.X;
                int square = height * width;
                MessageBox.Show($"{square}, {SelectedColor}");
            }
        }

        public void ResetColor()
        {
            IsSelected = false;
            SelectedColor = Color;
        }
    }
}

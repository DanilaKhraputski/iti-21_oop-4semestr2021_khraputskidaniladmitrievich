﻿using System;
using DatabaseLINQClassLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DatabaseLINQUnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethodFillDS()
        {
            MemosDS memoDS = new MemosDS();
            memoDS.Fill();

            object[] actual = memoDS.DS.Tables[0].Rows[1].ItemArray;
            Assert.AreEqual("qwe", actual[0].ToString());
        }

        [TestMethod]
        public void TestMethodAddRow()
        {
            MemosDS memoDS = new MemosDS();

            memoDS.Fill();
            int expected = memoDS.DS.Tables[0].Rows.Count + 1;

            memoDS.AddRow();
            int actual = memoDS.DS.Tables[0].Rows.Count;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMethodUpdateDB()
        {
            MemosDS memoDS = new MemosDS();

            memoDS.Fill();
            memoDS.AddRow();
            int expected = memoDS.DS.Tables[0].Rows.Count;

            memoDS.Update();
            int actual = memoDS.DS.Tables[0].Rows.Count;

            Assert.AreEqual(expected, actual);
        }
    }
}

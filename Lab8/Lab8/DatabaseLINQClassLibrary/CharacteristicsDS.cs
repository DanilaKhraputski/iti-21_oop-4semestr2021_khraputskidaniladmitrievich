﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLINQClassLibrary
{
    public class CharacteristicsDS : MyDS
    {
        public override void Fill()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                string sql = "select * from Characteristics";
                Adapter = new SqlDataAdapter(sql, connectionString);
                Adapter.Fill(DS);
            }
        }
    }
}

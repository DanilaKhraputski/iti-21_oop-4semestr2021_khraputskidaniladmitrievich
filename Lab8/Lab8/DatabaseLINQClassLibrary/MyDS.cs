﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLINQClassLibrary
{
    public abstract class MyDS : Connection
    {
        public DataSet DS { get; protected set; }
        protected SqlDataAdapter Adapter { get; set; }

        public MyDS()
        {
            DS = new DataSet();
        }

        public void AddRow()
        {
            DataRow row = DS.Tables[0].NewRow();
            DS.Tables[0].Rows.Add(row);
        }

        public abstract void Fill();

        public void Update()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(Adapter);
                Adapter.Update(DS);

                DS.Clear();
                Adapter.Fill(DS);
            }
        }

        public void Delete(int index)
        {
            DS.Tables[0].Rows[index].Delete();
        }
    }
}

﻿using DatabaseLINQClassLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab8
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CharacteristicsDS characteristicsDS;
        MemosDS memosDS;
        VegetableDS vegetablesDS;

        Dictionary<string, int> vegetable;
        Dictionary<string, int> memos;

        public MainWindow()
        {
            InitializeComponent();

            MemosDS_Init();
            CharacteristicsDS_Init();
            VegetablesDS_Init();

            UpdateCB();
        }

        private void MemosDS_Init()
        {
            memosDS = new MemosDS();
            memosDS.Fill();

            memoDG.ItemsSource = memosDS.DS.Tables[0].DefaultView;

            vegetable = new Dictionary<string, int>();
        }

        private void CharacteristicsDS_Init()
        {
            characteristicsDS = new CharacteristicsDS();
            characteristicsDS.Fill();

            VegetableCB.ItemsSource = vegetable;
            characteristicsDG.ItemsSource = characteristicsDS.DS.Tables[0].DefaultView;

            memos = new Dictionary<String, int>();
        }

        private void VegetablesDS_Init()
        {
            vegetablesDS = new VegetableDS();
            vegetablesDS.Fill();

            memoCB.ItemsSource = memos;
            vegetableDG.ItemsSource = vegetablesDS.DS.Tables[0].DefaultView;
        }

        private void addRow_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                switch (tabs.SelectedIndex)
                {
                    case 0:
                        characteristicsDS.AddRow();
                        break;
                    case 1:
                        vegetablesDS.AddRow();
                        break;
                    case 2:
                        memosDS.AddRow();
                        break;
                }
            }
            catch
            {
                MessageBox.Show("Ошибка при добавлении записи.");
            }
            UpdateTables();
        }

        private void delRow_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                switch (tabs.SelectedIndex)
                {
                    case 0:
                        characteristicsDS.Delete(characteristicsDG.SelectedIndex);
                        break;
                    case 1:
                        vegetablesDS.Delete(vegetableDG.SelectedIndex);
                        break;
                    case 2:
                        memosDS.Delete(memoDG.SelectedIndex);
                        break;
                }
            }
            catch
            {
                MessageBox.Show("Ошибка при добавлении записи.");
            }
            UpdateTables();
        }

        private void updDB_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                characteristicsDS.Update();
                vegetablesDS.Update();
                memosDS.Update();
                MessageBox.Show("Данные успешно обновлены.");
            }
            catch
            {
                MessageBox.Show("Ошибка при добавлении записи.");
            }
            UpdateTables();
            UpdateCB();

        }

        private void UpdateTables()
        {
            characteristicsDG.ItemsSource = characteristicsDS.DS.Tables[0].DefaultView;
            vegetableDG.ItemsSource = vegetablesDS.DS.Tables[0].DefaultView;
            memoDG.ItemsSource = memosDS.DS.Tables[0].DefaultView;
        }

        private void UpdateCB()
        {
            memos.Clear();
            foreach (DataRow row in memosDS.DS.Tables[0].Rows)
            {
                object[] arr = row.ItemArray;
                memos.Add(arr[0].ToString(), (int)arr[1]);
            }

            vegetable.Clear();
            foreach (DataRow row in vegetablesDS.DS.Tables[0].Rows)
            {
                object[] arr = row.ItemArray;
                vegetable.Add(arr[1].ToString(), (int)arr[0]);
            }

            VegetableCB.ItemsSource = vegetable;
            memoCB.ItemsSource = memos;
        }
    }
}

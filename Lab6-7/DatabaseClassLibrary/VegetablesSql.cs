﻿using DatabaseClassLibrary.Object;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseClassLibrary
{
    public class VegetablesSql : Sql
    {
        public List<Vegetables> Vegetables { get; private set; }

        public Vegetables this[Int32 index] { get { return Vegetables[index]; } }

        public VegetablesSql(String connectionString) : base(connectionString) { }

        public override void Delete(int index)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                String sql = $"delete from Vegetables where VegetableId = {Vegetables[index].VegetableId}";
                SqlCommand command = new SqlCommand(sql, connection);
                Int32 number = command.ExecuteNonQuery();
                if (number == 0) throw new Exception();
            }
        }

        public override void Read()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = @"select * from Vegetables";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    Vegetables = new List<Vegetables>();
                    while (reader.Read())
                    {
                        Vegetables.Add(new Vegetables
                        {
                            VegetableId = reader.GetInt32(0),                      
                            NameVegetable = reader.GetString(1),
                            NameSort = reader.GetString(2),
                            MemoId = reader.GetInt32(3)
                        });
                    }
                }
            }
        }

        public override void Update()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                String sql;
                SqlCommand command;
                foreach (Vegetables vegetable in Vegetables)
                {
                    sql = $@"update Vegetables set MemoId = {vegetable.MemoId}, " +
                       $"NameVegetable = '{vegetable.NameVegetable}', " +
                       $"NameSort = '{vegetable.NameSort}' " +
                       $"where VegetableId = {vegetable.VegetableId}";
                    command = new SqlCommand(sql, connection);
                    Int32 number = command.ExecuteNonQuery();
                    if (number == 0) throw new Exception();
                }
            }
        }

        public void Insert( String nameVegetable, String nameSort, Int32 memoId)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                string sql = "insert Vegetables(NameVegetable, NameSort, MemoId) values " +
                    $"('{nameVegetable}', '{nameSort}', {memoId})";
                SqlCommand command = new SqlCommand(sql, connection);
                Int32 number = command.ExecuteNonQuery();
                if (number == 0) throw new Exception();
            }
        }
    }
}

﻿using DatabaseClassLibrary.Object;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseClassLibrary
{
    public class CharacteristicsSql : Sql
    {
        public List<Characteristics> Characteristics { get; private set; }

        public Characteristics this[Int32 index] { get { return Characteristics[index]; } }

        public CharacteristicsSql(String connectionString) : base(connectionString) { }

        public override void Delete(int index)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                String sql = $"delete from Characteristics where CharacteristicsId = {Characteristics[index].CharacteristicsId}";
                SqlCommand command = new SqlCommand(sql, connection);
                Int32 number = command.ExecuteNonQuery();
                if (number == 0) throw new Exception();
            }
        }

        public override void Read()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = @"select * from Characteristics";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    Characteristics = new List<Characteristics>();
                    while (reader.Read())
                    {
                        Characteristics.Add(new Characteristics
                        {
                            CharacteristicsId = reader.GetInt32(0),
                            Height = reader.GetInt32(1),
                            RecommendedPickUpDate = reader.GetInt32(2),
                            PickUpTimeHarvest = reader.GetInt32(3),
                            VegetableId = reader.GetInt32(4)

                        });
                    }
                }
            }
        }

        public override void Update()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                String sql;
                SqlCommand command;
                foreach (Characteristics characteristic in Characteristics)
                {
                    sql = $@"update Characteristics set VegetableId = {characteristic.VegetableId}, " +
                       $"Height = {characteristic.Height}, " +
                       $"RecommendedPickUpDate = {characteristic.RecommendedPickUpDate}, " +
                       $"PickUpTimeHarvest = {characteristic.PickUpTimeHarvest} " +       
                       $"where CharacteristicsId = {characteristic.CharacteristicsId}";
                    command = new SqlCommand(sql, connection);
                    Int32 number = command.ExecuteNonQuery();
                    if (number == 0) throw new Exception();
                }
            }
        }

        public void Insert(Int32 vegetableId, Int32 pickUpTimeHarvest, Int32 recommendedPickUpDate, Int32 height)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                string sql = "insert Characteristics(VegetableId, PickUpTimeHarvest, RecommendedPickUpDate, Height) values " +
                    $"({vegetableId}, {pickUpTimeHarvest}, {recommendedPickUpDate}, {height})";
                SqlCommand command = new SqlCommand(sql, connection);
                Int32 number = command.ExecuteNonQuery();
                if (number == 0) throw new Exception();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseClassLibrary.Object
{
    public class Characteristics
    {
        public Int32 CharacteristicsId { get; set; }
        public Int32 Height { get; set; }
        public Int32 RecommendedPickUpDate { get; set; }
        public Int32 PickUpTimeHarvest { get; set; }
        public Int32 VegetableId { get; set; }
    }
}

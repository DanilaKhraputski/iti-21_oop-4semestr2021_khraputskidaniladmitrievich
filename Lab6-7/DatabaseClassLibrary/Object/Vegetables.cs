﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseClassLibrary.Object
{
    public class Vegetables
    {
        public Int32 VegetableId { get; set; }
        public String NameVegetable { get; set; }
        public String NameSort { get; set; }
        public Int32 MemoId { get; set; }
    }
}

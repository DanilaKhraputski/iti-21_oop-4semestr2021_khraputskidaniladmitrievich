﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseClassLibrary.Object
{
    public class Memos
    {
        public Int32 MemoId { get; set; }
        public String MemoName { get; set; }
    }
}

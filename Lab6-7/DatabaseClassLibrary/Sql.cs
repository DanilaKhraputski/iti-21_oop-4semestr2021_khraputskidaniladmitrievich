﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseClassLibrary
{
    public abstract class Sql
    {
        protected String connectionString;

        public Sql(String connectionString)
        {
            this.connectionString = connectionString;
        }

        public abstract void Read();
        public abstract void Update();
        public abstract void Delete(Int32 index);
    }
}

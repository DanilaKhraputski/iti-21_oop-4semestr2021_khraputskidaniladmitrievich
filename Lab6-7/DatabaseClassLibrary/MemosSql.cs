﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using DatabaseClassLibrary.Object;

namespace DatabaseClassLibrary
{
    public class MemosSql : Sql
    {
        public List<Memos> Memos { get; private set; }

        public Memos this[Int32 index] { get { return Memos[index]; } }

        public MemosSql(String connectionString) : base(connectionString) { }

        public override void Delete(int index)
        {
            using(SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                String sql = $"delete from Memos where MemoId = {Memos[index].MemoId}";
                SqlCommand command = new SqlCommand(sql, connection);
                Int32 number = command.ExecuteNonQuery();
                if (number == 0) throw new Exception();
            }
        }

        public override void Read()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = @"select * from Memos";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    Memos = new List<Memos>();
                    while (reader.Read())
                    {
                        Memos.Add(new Memos
                        {
                            MemoName = reader.GetString(0),
                            MemoId = reader.GetInt32(1)
                            
                        });
                    }
                }
            }
        }

        public override void Update()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                String sql;
                SqlCommand command;
                foreach (Memos memo in Memos)
                {
                    sql = $@"update Memos set NameMemo = '{memo.MemoName}'" +
                        $"where MemoId = '{memo.MemoId}'";
                    command = new SqlCommand(sql, connection);
                    Int32 number = command.ExecuteNonQuery();
                    if (number == 0) throw new Exception();
                }
            }
        }

        public void Insert(String memoName)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                String sql = "insert Memos(NameMemo) values " +
                    $"('{memoName}')";
                SqlCommand command = new SqlCommand(sql, connection);
                Int32 number = command.ExecuteNonQuery();
                if (number == 0) throw new Exception();
            }
        }
    }
}

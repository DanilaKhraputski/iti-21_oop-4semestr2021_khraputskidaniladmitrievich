﻿using DatabaseClassLibrary;
using DatabaseClassLibrary.Object;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab6_7
{
    public partial class Form1 : Form
    {
        private String connectionString = ConnectionString.connectionString;
        public CharacteristicsSql characteristicsSql;
        public MemosSql memosSql;
        public VegetablesSql vegetablesSql;

        Memos memos;
        Characteristics characteristics;
        Vegetables vegetables;

        public Form1()
        {
            InitializeComponent();

            memosSql = new MemosSql(connectionString);
            memosSql.Read();
            characteristicsSql = new CharacteristicsSql(connectionString);
            characteristicsSql.Read();
            vegetablesSql = new VegetablesSql(connectionString);
            vegetablesSql.Read();
            VegetableDGV_Init();
            MemosDGV_Init();
            CharacteristicsDGV_Init();
            AddingObjects_Init();
        }

        private void CharacteristicsDGV_Init()
        {
            characteristicsDGV.AutoGenerateColumns = false;
            characteristicsDGV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            characteristicsDGV.MultiSelect = false;

            vegetableIdCB.AutoComplete = true;
            vegetableIdCB.DataPropertyName = "VegetableId";
            vegetableIdCB.ValueMember = "VegetableId";
            vegetableIdCB.DisplayMember = "VegetableId";
            vegetableIdCB.DataSource = vegetablesSql.Vegetables;

            characteristicsDGV.DataSource = characteristicsSql.Characteristics;
        }

        private void VegetableDGV_Init()
        {
            vegetableDGV.AutoGenerateColumns = false;
            vegetableDGV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            vegetableDGV.MultiSelect = false;

            memoCB.AutoComplete = true;
            memoCB.DataPropertyName = "MemoId";
            memoCB.ValueMember = "MemoId";
            memoCB.DisplayMember = "MemoName";
            memoCB.DataSource = memosSql.Memos;

            vegetableDGV.DataSource = vegetablesSql.Vegetables;
        }

        private void MemosDGV_Init()
        {
            memosDGV.AutoGenerateColumns = false;
            memosDGV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            memosDGV.MultiSelect = false;

            memosDGV.DataSource = memosSql.Memos;
        }
        private void addRowButton_Click(object sender, EventArgs e)
        {
            try
            {
                switch (tabControl.SelectedIndex)
                {
                    case 0:
                        characteristicsSql.Characteristics.Add(characteristics);
                        characteristicsSql.Insert(characteristics.VegetableId, characteristics.PickUpTimeHarvest, characteristics.RecommendedPickUpDate, characteristics.Height);
                        break;
                    case 1:
                        vegetablesSql.Vegetables.Add(vegetables);
                        vegetablesSql.Insert(vegetables.NameVegetable, vegetables.NameSort, vegetables.MemoId);
                        break;
                    case 2:
                        memosSql.Memos.Add(memos);
                        memosSql.Insert(memos.MemoName);
                        break;
                }
            }
            catch
            {
                MessageBox.Show("Ошибка при добавлении записи.");
            }
            finally
            {
                UpdateTables();
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                switch (tabControl.SelectedIndex)
                {
                    case 0:
                        characteristicsSql.Delete(characteristicsDGV.SelectedRows[0].Index);
                        break;
                    case 1:
                        vegetablesSql.Delete(vegetableDGV.SelectedRows[0].Index);
                        break;
                    case 2:
                        memosSql.Delete(memosDGV.SelectedRows[0].Index);
                        break;
                }
            }
            catch
            {
                MessageBox.Show("Ошибка при удалении записи.");
            }
            finally
            {
                UpdateTables();
            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            try
            {
                switch (tabControl.SelectedIndex)
                {
                    case 0:
                        characteristicsSql.Update();
                        break;
                    case 1:
                        vegetablesSql.Update();
                        break;
                    case 2:
                        memosSql.Update();
                        break;
                }
                MessageBox.Show("Обновление упешно завершено.");
            }
            catch
            {
                MessageBox.Show("Ошибка при обновлении записей.");
            }
            finally
            {
                UpdateTables();
            }
        }

        private void UpdateTables()
        {
            memosSql.Read();
            vegetablesSql.Read();
            characteristicsSql.Read();

            UpdateCBTables();

            memosDGV.DataSource = memosSql.Memos;
            characteristicsDGV.DataSource = characteristicsSql.Characteristics;
            vegetableDGV.DataSource = vegetablesSql.Vegetables;
        }

        private void UpdateCBTables()
        {
            vegetableIdCB.DataSource = vegetablesSql.Vegetables;
            memoCB.DataSource = memosSql.Memos;
        }

        private void AddingObjects_Init()
        {
            characteristics = new Characteristics
            {                
                Height = 1,
                RecommendedPickUpDate = 1,
                PickUpTimeHarvest = 1,
                VegetableId = vegetablesSql[0].VegetableId                
            };

            vegetables = new Vegetables
            {
                MemoId = memosSql[0].MemoId,
                NameVegetable = " ",
                NameSort = " "
            };

            memos = new Memos { MemoName = " " };
        }

    }
}

﻿namespace Lab6_7
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.updateButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.addRowButton = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.characteristicsDGV = new System.Windows.Forms.DataGridView();
            this.HeightTB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecommendedPickUpDateTB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PickUpTimeHarvestTB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vegetableIdCB = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.vegetableDGV = new System.Windows.Forms.DataGridView();
            this.memoCB = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.nameVegetableTB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameSortTB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.memosDGV = new System.Windows.Forms.DataGridView();
            this.memoTB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.characteristicsDGV)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vegetableDGV)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memosDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(658, 394);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(126, 44);
            this.updateButton.TabIndex = 7;
            this.updateButton.Text = "Обновление БД";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(150, 394);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(126, 44);
            this.deleteButton.TabIndex = 6;
            this.deleteButton.Text = "Удалить запись";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // addRowButton
            // 
            this.addRowButton.Location = new System.Drawing.Point(18, 394);
            this.addRowButton.Name = "addRowButton";
            this.addRowButton.Size = new System.Drawing.Size(126, 44);
            this.addRowButton.TabIndex = 5;
            this.addRowButton.Text = "Добавить запись";
            this.addRowButton.UseVisualStyleBackColor = true;
            this.addRowButton.Click += new System.EventHandler(this.addRowButton_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(776, 380);
            this.tabControl.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.characteristicsDGV);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(768, 354);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Характеристики";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // characteristicsDGV
            // 
            this.characteristicsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.characteristicsDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HeightTB,
            this.RecommendedPickUpDateTB,
            this.PickUpTimeHarvestTB,
            this.vegetableIdCB});
            this.characteristicsDGV.Location = new System.Drawing.Point(6, 6);
            this.characteristicsDGV.Name = "characteristicsDGV";
            this.characteristicsDGV.Size = new System.Drawing.Size(756, 342);
            this.characteristicsDGV.TabIndex = 0;
            // 
            // HeightTB
            // 
            this.HeightTB.DataPropertyName = "Height";
            this.HeightTB.HeaderText = "Высота";
            this.HeightTB.Name = "HeightTB";
            // 
            // RecommendedPickUpDateTB
            // 
            this.RecommendedPickUpDateTB.DataPropertyName = "RecommendedPickUpDate";
            this.RecommendedPickUpDateTB.HeaderText = "Рекомендация";
            this.RecommendedPickUpDateTB.Name = "RecommendedPickUpDateTB";
            // 
            // PickUpTimeHarvestTB
            // 
            this.PickUpTimeHarvestTB.DataPropertyName = "PickUpTimeHarvest";
            this.PickUpTimeHarvestTB.HeaderText = "Посадка";
            this.PickUpTimeHarvestTB.Name = "PickUpTimeHarvestTB";
            // 
            // vegetableIdCB
            // 
            this.vegetableIdCB.HeaderText = "Овощ";
            this.vegetableIdCB.Name = "vegetableIdCB";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.vegetableDGV);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(768, 354);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Овощи";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // vegetableDGV
            // 
            this.vegetableDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.vegetableDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.memoCB,
            this.nameVegetableTB,
            this.nameSortTB});
            this.vegetableDGV.Location = new System.Drawing.Point(6, 6);
            this.vegetableDGV.Name = "vegetableDGV";
            this.vegetableDGV.Size = new System.Drawing.Size(756, 342);
            this.vegetableDGV.TabIndex = 1;
            // 
            // memoCB
            // 
            this.memoCB.HeaderText = "Памятка";
            this.memoCB.Name = "memoCB";
            this.memoCB.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.memoCB.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.memoCB.Width = 300;
            // 
            // nameVegetableTB
            // 
            this.nameVegetableTB.DataPropertyName = "NameVegetable";
            this.nameVegetableTB.HeaderText = "Название овоща";
            this.nameVegetableTB.Name = "nameVegetableTB";
            // 
            // nameSortTB
            // 
            this.nameSortTB.DataPropertyName = "NameSort";
            this.nameSortTB.HeaderText = "Сорт";
            this.nameSortTB.Name = "nameSortTB";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.memosDGV);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(768, 354);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Памятки";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // memosDGV
            // 
            this.memosDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.memosDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.memoTB});
            this.memosDGV.Location = new System.Drawing.Point(6, 6);
            this.memosDGV.Name = "memosDGV";
            this.memosDGV.Size = new System.Drawing.Size(756, 342);
            this.memosDGV.TabIndex = 2;
            // 
            // memoTB
            // 
            this.memoTB.DataPropertyName = "MemoName";
            this.memoTB.HeaderText = "Памятка";
            this.memoTB.Name = "memoTB";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.addRowButton);
            this.Controls.Add(this.tabControl);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.characteristicsDGV)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vegetableDGV)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memosDGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button addRowButton;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView characteristicsDGV;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView vegetableDGV;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView memosDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn memoTB;
        private System.Windows.Forms.DataGridViewTextBoxColumn HeightTB;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecommendedPickUpDateTB;
        private System.Windows.Forms.DataGridViewTextBoxColumn PickUpTimeHarvestTB;
        private System.Windows.Forms.DataGridViewComboBoxColumn vegetableIdCB;
        private System.Windows.Forms.DataGridViewComboBoxColumn memoCB;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameVegetableTB;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameSortTB;
    }
}


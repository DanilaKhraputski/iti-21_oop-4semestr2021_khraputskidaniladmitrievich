﻿using System;
using DatabaseClassLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        private static string connectionString = ConnectionString.connectionString;

        [TestMethod]
        public void TestMethodReadWorker()
        {
            MemosSql memosSql = new MemosSql(connectionString);
            memosSql.Read();
            Assert.IsNotNull(memosSql.Memos);
        }

        [TestMethod]
        public void TestMethodInsertWorker()
        {
            MemosSql memosSql = new MemosSql(connectionString);

            memosSql.Read();
            int predicated = memosSql.Memos.Count + 1;

            memosSql.Insert("test");

            memosSql.Read();
            int actual = memosSql.Memos.Count;

            Assert.AreEqual(predicated, actual);
        }

        [TestMethod]
        public void TestMethodUpdateWorkers()
        {
            MemosSql memosSql = new MemosSql(connectionString);

            memosSql.Read();

            memosSql[memosSql.Memos.Count - 1].MemoName = "newTest";

            memosSql.Update();

            memosSql.Read();

            Assert.AreEqual(memosSql[memosSql.Memos.Count - 1].MemoName, "newTest");
        }

        [TestMethod]
        public void TestMethodDeleteWorker()
        {
            MemosSql memosSql = new MemosSql(connectionString);

            memosSql.Read();

            memosSql.Delete(memosSql.Memos.Count - 1);

            memosSql.Read();

            Assert.AreNotEqual(memosSql[memosSql.Memos.Count - 1].MemoName, "newTest");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lib4
{
    public class StringCollection
    {
        public delegate void DelegateMethod(String str);
        
        public event DelegateMethod Notify;

        public List<String> Strings { get; }

        public StringCollection()
        {
            Strings = new List<String>();
        }

        public int GetCollectionLength { get { return Strings.Count; } }

        public bool ReadFile(String filePath)
        {
            
            Strings.Clear();
            Boolean flag = true;
            try
            {
                using (StreamReader fileStream = new StreamReader(filePath))
                {
                    string text = fileStream.ReadToEnd();

                    text = text.Replace('\n', ' ');

                    string[] textArr = text.Split(' ');

                    for (int i = 0; i < textArr.Length; i++)
                    {
                        this.Strings.Add(textArr[i] + ' ');
                    }

                    if (textArr.Length == Strings.Count)
                        Notify?.Invoke("Чтение из файла завершено успешно.");
                    else
                    {
                        Notify?.Invoke("Чтение из файла завершено с ошибками.");
                        flag = false;
                    }
                }
            }
            catch
            {
                return false;
            }
            return flag;
        }
    }
}

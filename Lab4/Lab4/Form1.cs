﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using Lib4;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab4
{
    public partial class Form1 : Form
    {
        public event Action ClearChart;

        StringCollection stringCollection;

        Button buttonSaveFile;

        TextBox textBox;

        Button buttonAddFile;

        public Form1()
        {
            Action clearTextBox = () => { textBox.Clear(); };

            ClearChart += clearTextBox;

            Width = 500;
            Height = 500;

            buttonAddFile = new Button()
            {
                Location = new Point(12, 34),
                Name = "buttonAddFile",
                Size = new Size(100, 23),
                Text = "Add file",
                UseVisualStyleBackColor = true
            };

             buttonSaveFile = new Button()
            {
                Location = new Point(120, 34),
                Name = "buttonSaveFile",
                Size = new Size(100, 23),
                Text = "Save file",
                UseVisualStyleBackColor = true
            };

            textBox = new TextBox()
            {
                Location = new Point(12, 60),
                Name = "textBox",
                Size = new Size(300, 300),
                Text = "Save file",
                Multiline = true,
                ScrollBars = ScrollBars.Vertical
            };

            textBox.KeyDown += textBox1_KeyDown;

            stringCollection = new StringCollection();
            stringCollection.Notify += ShowMessage;

            buttonAddFile.Click += new EventHandler(addFile_Click);

            buttonSaveFile.Click += new EventHandler(saveFile_Click);

            Controls.Add(buttonAddFile);
            Controls.Add(buttonSaveFile);
            Controls.Add(textBox);

            InitializeComponent();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.D))
            {
                textBox.Text = new string(textBox.Text.Reverse().SkipWhile(x => x != ' ').Reverse().ToArray()).TrimEnd(' ');              
                textBox.SelectionStart = textBox.Text.Length;
                textBox.ScrollToCaret();
            }
        }

        public void addFile_Click(object sender, EventArgs e)
        {
            if (stringCollection.ReadFile(@"C:\Users\xddan\OneDrive\Рабочий стол\test.txt"))
            {
                ClearChart?.Invoke();
                foreach (String strings in stringCollection.Strings)
                {
                    textBox.Text += strings;
                }
               
            }
        }

        private void ShowMessage(string str)
        {
            MessageBox.Show(str);
        }
        public void saveFile_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Текстовый документ (*.txt)|*.txt|Все файлы (*.*)|*.*";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter streamWriter = new StreamWriter(saveFileDialog.FileName);
                streamWriter.WriteLine(textBox.Text);
                streamWriter.Close();
            }
        }
        
}
}

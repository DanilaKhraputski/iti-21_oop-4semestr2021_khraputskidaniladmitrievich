﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lib4;
using System.Collections.Generic;

namespace UnitTest
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestMethodReadFile1()
        {
            StringCollection stringCollection = new StringCollection();
            //List<String> stinrg = new List<String>();
            Boolean expected = stringCollection.ReadFile(@"C:\Users\xddan\OneDrive\Рабочий стол\test.txt");
            Assert.IsTrue(expected);
        }

        [TestMethod]
        public void TestMethodReadFile2()
        {
            StringCollection stringCollection1 = new StringCollection();
            //List<String> stinrg = new List<String>();
            Boolean expected = stringCollection1.ReadFile(@"C:\Users\xddan\OneDrive\Рабочий стол\test");
            Assert.IsFalse(expected);
        }

        [TestMethod]
        public void TestMethodReadFile3()
        {
            StringCollection stringCollection1 = new StringCollection();
            //List<String> stinrg = new List<String>();
            Boolean expected = stringCollection1.ReadFile(@"C:\Users\xddan\OneDrive\Рабочий стол\OOP\Lab4\sourse\TestText.txt");
            Assert.IsTrue(expected);
        }
    }
}

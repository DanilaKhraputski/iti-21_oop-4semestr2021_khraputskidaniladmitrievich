﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace ClassLibraryStreamDecoder
{
    /// <summary>
    /// класс декоратор Stream
    /// </summary>
    public class WriteOverrideStream : Stream
    {
        private readonly Stream _stream;
        

        /// <summary>
        /// конструктор класс 
        /// </summary>
        /// <param name="stream">стрим</param>
       
        public WriteOverrideStream(Stream stream)
        {
            this._stream = stream;
          
        }

        /// <summary>
        /// метод получения пароля
        /// </summary>
        /// <returns></returns>
  
        /// <summary>
        /// переопределение stream метода CanRead
        /// </summary>
        public override bool CanRead => _stream.CanRead;
        /// <summary>
        /// переопределение stream метода CanSeek
        /// </summary>
        public override bool CanSeek => _stream.CanSeek;
        /// <summary>
        /// переопределение stream метода CanWrite
        /// </summary>
        public override bool CanWrite => _stream.CanWrite;
        /// <summary>
        /// переопределение stream метода Length
        /// </summary>
        public override long Length => _stream.Length;
        /// <summary>
        /// переопределение stream метода Position
        /// </summary>
        public override long Position { get => _stream.Position; set => _stream.Position = value; }
        /// <summary>
        /// переопределение stream метода Flush
        /// </summary>
        public override void Flush()
        {
            _stream.Flush();
        }
        /// <summary>
        /// переопределение stream метода Read
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public override int Read(byte[] buffer, int offset, int count)
        {
            return _stream.Read(buffer, offset, count);
        }
        /// <summary>
        ///  переопределение stream метода Seek
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        public override long Seek(long offset, SeekOrigin origin)
        {
            return _stream.Seek(offset, origin);
        }

        /// <summary>
        /// переопределение stream метода SetLength
        /// </summary>
        /// <param name="value"></param>
        public override void SetLength(long value)
        {
            _stream.SetLength(value);
        }
        /// <summary>
        /// авто-свойства для получения и присвоения записи в поток
        /// </summary>
        public List<byte> StringList { get; set; }
        /// <summary>
        /// переопределение stream метода Write
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        public override void Write(byte[] buffer, int offset, int count)
        {
           
                StringList = buffer.ToList();
                _stream.Write(buffer, offset, count);
        }
    }
}
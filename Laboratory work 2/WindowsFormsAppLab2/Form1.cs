﻿using ClassLibraryStreamDecoder;
using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsAppLab2
{
    /// <summary>
    /// класс формы
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class Form1 : Form
    {
        private OpenFileDialog openFile = new OpenFileDialog();
        private Stream fileStream;
        private Stream bufferStream;
        private Stream memoryStream;
        private WriteOverrideStream decorator;
        private WriteOverrideStream decoratorBuffer;
        private WriteOverrideStream decoratorMemory;
        private StreamReader myReader;
        /// <summary>
        /// Initializes a new instance of the <see cref="Form1"/> class.
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            listBox4.Items.Clear();
            
            openFile.Filter = "TXT файл(*.txt)|*.txt";
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                fileStream = new FileStream(openFile.FileName, FileMode.Open);
                bufferStream = new BufferedStream(fileStream);
                memoryStream = new MemoryStream();
                decorator = new WriteOverrideStream(fileStream);
                decoratorBuffer = new WriteOverrideStream(bufferStream);
                decoratorMemory = new WriteOverrideStream(memoryStream);
                myReader = new StreamReader(fileStream);
            }
        }

        

        private void UpdateListBox()
        {
            try
            {
                string line;
                
                while (!myReader.EndOfStream)
                {
                    if ((line = myReader.ReadLine()) != null)
                    {
                        byte[] array = System.Text.Encoding.UTF8.GetBytes(line);
                        
                        decorator.Write(array, 0, array.Length);
                        decoratorBuffer.Write(array, 0, array.Length);
                        decoratorMemory.Write(array, 0, array.Length);
                        
                        string newBufferStrung = BitConverter.ToString(decoratorBuffer.StringList.ToArray());
                        string newMemoryString = BitConverter.ToString(decoratorMemory.StringList.ToArray());
                        string newDecorator = BitConverter.ToString(decorator.StringList.ToArray());
                        listBox3.Items.Add(newMemoryString);
                        listBox4.Items.Add(line);
                        listBox1.Items.Add(newDecorator);
                        listBox2.Items.Add(newBufferStrung);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        

        

        private void Write_Click(object sender, EventArgs e)
        {
            UpdateListBox();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            listBox4.Items.Clear();

            openFile.Filter = "TXT файл(*.txt)|*.txt";
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                fileStream = new FileStream(openFile.FileName, FileMode.Open);
                bufferStream = new BufferedStream(fileStream);
                memoryStream = new MemoryStream();
                decorator = new WriteOverrideStream(fileStream);
                decoratorBuffer = new WriteOverrideStream(bufferStream);
                decoratorMemory = new WriteOverrideStream(memoryStream);
                myReader = new StreamReader(fileStream);
            }
        }
    }
}